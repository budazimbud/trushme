import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { BaseRepository } from "../../base/base.respository";
import { User, UserDocument } from "../entities/user.entity";

@Injectable()
export class UserRepository extends BaseRepository<UserDocument>{
    constructor(@InjectModel(User.name) private readonly userModel : Model <UserDocument>){
        super(userModel)
    }
}