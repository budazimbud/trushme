import { IsEmail, IsEmpty, IsString, MinLength } from "class-validator"

export class CreateUserDto {
    @IsString()
    fullName:string

    @IsEmail()
    email:string

    @IsString()
    @MinLength(8)
    password:string

    @IsString()
    phoneNumber:string

    @IsEmpty()
    isVerified:boolean


}
