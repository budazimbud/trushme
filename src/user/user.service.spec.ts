import { Test, TestingModule } from '@nestjs/testing';
import { UserRepository } from './repository/user.repository';
import { UserService } from './user.service';

describe('UserService', () => {
  let userService: UserService;
  let userRepository:UserRepository

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserService,{
        provide:UserRepository , useFactory : ()=>({
          find: jest.fn(()=>[]),
          findOne: jest.fn(()=>[]),
          findOneAndUpdate: jest.fn(()=>[]),
          save: jest.fn(()=>{}),
        })
      }],
    }).compile();

    userService = module.get<UserService>(UserService);
    userRepository = module.get<UserRepository>(UserRepository);

  });

  it('should be defined', () => {
    expect(userService).toBeDefined();
  });
});
