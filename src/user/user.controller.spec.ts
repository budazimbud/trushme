import { Test, TestingModule } from '@nestjs/testing';
import { UserRepository } from './repository/user.repository';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService,
      {
        provide:UserRepository , useFactory : ()=>({
          find: jest.fn(()=>[]),
          findOne: jest.fn(()=>[]),
          findOneAndUpdate: jest.fn(()=>[]),
          save: jest.fn(()=>{}),
        })
      }],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
