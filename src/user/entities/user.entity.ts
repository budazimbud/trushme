import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";

export type UserDocument = User & Document

@Schema({timestamps:{updatedAt:'lastLogin'}})
export class User {

    id:string

    @Prop({required:true})
    fullName:string

    @Prop({required:true})
    email:string

    @Prop({required:true})
    password:string

    @Prop({required:true})
    phoneNumber:string

    
}

export const UserSchema = SchemaFactory.createForClass(User)