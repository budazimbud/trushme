import { FilterQuery, Model, UpdateQuery } from 'mongoose';

export abstract class BaseRepository<T extends Document> {
    constructor(protected readonly entityModel: Model<T>){}

    async findOne(
        entityFilterQuery: FilterQuery<T>,
        projection?: Record<string, unknown>
    ): Promise<T | null>{
        return this.entityModel.findOne(entityFilterQuery, {
            _id: 1,
            __v: 0,
            ...projection
        }).exec();
    }

    async find(entityFilterQuery?: FilterQuery<T>, limit?:number): Promise<T[] | null>{
        return this.entityModel.find(entityFilterQuery).limit(limit);
    }

    async save(createEntityData: any): Promise<T>{
        const entity = new this.entityModel(createEntityData);
        return entity.save();
    }

    async findOneAndUpdate(entityFilterQuery: FilterQuery<T>, updateEntityData: UpdateQuery<unknown>): Promise<T | null>{
        return this.entityModel.findOneAndUpdate(entityFilterQuery, updateEntityData, {new: true})
    }

}
