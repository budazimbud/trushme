import { Injectable } from '@nestjs/common';
import { MailgunService } from '@nextnm/nestjs-mailgun';
import { EmailOptions } from './email-options.interface';

@Injectable()
export class MailService {
    options: EmailOptions 

    constructor(private mailgunService: MailgunService){
        this.options = {
            from: 'budazimbud@gmail.com',
            to: '',
            subject: 'confirm email',
            text: 'hallo',
            html: '',
            attachment:'',
            'h:X-Mailgun-Variables': '{"key":"value"}'
          };
    }

    async sendEmail(option:EmailOptions){
        try{
            return await this.mailgunService.sendEmail(option);
        }catch{
            console.log("OK");
        }

    }
}
