import { Module } from '@nestjs/common';
import { MailgunModule } from '@nextnm/nestjs-mailgun';
import { MailService } from './mail.service';

@Module({
    imports:[ MailgunModule.forRoot({
        DOMAIN: 'your domain',
        API_KEY: 'your api key',
        HOST: 'your host', // default: 'api.mailgun.net'. Note that if you are using the EU region the host should be set to 'api.eu.mailgun.net'
      })],
    providers: [MailService],
    exports:[MailService]
})
export class MailModule {}
