import { Injectable } from '@nestjs/common';
import {MailService} from './../../mail/mail.service';

@Injectable()
export class AuthMailService {
    constructor(private mailService:MailService){}

    async sendEmailforget(email:string,randomstring:string){

        this.mailService.options.subject = 'forget password'

        console.log(`mengirim kepada ${email}`)

        this.mailService.options.to = email

        this.mailService.options.html = `<a href="http://localhost:8080/auth/account/reset-password/${randomstring}">Confirm</a>`
        
        await this.mailService.sendEmail(this.mailService.options);
    }

    async sendEmail(email:string,randomstring:string){
       
        this.mailService.options.subject = 'verifilakasi email'
       
        console.log(`mengirim kepada ${email}`)
       
        this.mailService.options.to = email
       
        this.mailService.options.html = `<a href="http://localhost:8080/auth/account/confirm/${randomstring}">Confirm</a>`
       
        await this.mailService.sendEmail(this.mailService.options);
      }

}
